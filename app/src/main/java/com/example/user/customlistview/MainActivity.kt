package com.example.user.customlistview

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ListView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var listView = findViewById<ListView>(R.id.listview)
        var list = mutableListOf<Model>()

        list.add(Model("Whats app", "Whats app application...", R.drawable.whatsapp))
        list.add(Model("Facebook", "Facebook application...", R.drawable.facebook))
        list.add(Model("Instagram", "Instagram application...", R.drawable.instagram))
        list.add(Model("Snapchat", "Snapchat application...", R.drawable.snapchat))
        list.add(Model("Twitter", "Twitter application...", R.drawable.twitter))

        listView.adapter = MyListAdapter(this, R.layout.row, list)

        listView.setOnItemClickListener { parent, view, position, id ->
            if(position==0){
                Toast.makeText(this@MainActivity, "Whats app Clicked", Toast.LENGTH_SHORT).show()
            }
            if(position==1){
                Toast.makeText(this@MainActivity, "Facebook Clicked", Toast.LENGTH_SHORT).show()
            }
            if(position==2){
                Toast.makeText(this@MainActivity, "Instagram Clicked", Toast.LENGTH_SHORT).show()
            }
            if(position==3){
                Toast.makeText(this@MainActivity, "Snapchat Clicked", Toast.LENGTH_SHORT).show()
            }
            if(position==4){
                Toast.makeText(this@MainActivity, "Twitter Clicked", Toast.LENGTH_SHORT).show()
            }
        }

    }
}
